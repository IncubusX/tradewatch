package org.trade

import akka.actor.ActorSystem
import com.ning.http.client.ProxyServer
import dispatch.Defaults._
import dispatch._

import scala.concurrent.duration._
import scala.util.Random
import scala.io.Source

/**
 *
 * @author hbm50006
 */
case class Proxy(ip:String, port:Int)
object ProxyFinder {

    val rand = new Random()
    val http = Http.configure{builder =>
        builder.setFollowRedirects(true).setRequestTimeoutInMs(5000).setConnectionTimeoutInMs(5000)
    }

    var minimumProxies = 0
    var maximumProxies = minimumProxies + 30
    var proxyList: List[Proxy] = List[Proxy]()

    def invalidateProxy(proxy: Proxy) {
        println("Invalidating proxy.", proxy.ip, proxy.port)
        proxyList = proxyList.filter(_ != proxy)
    }

    def findMoreProxies() {
        val req = url("http://poe.trade/search").POST
        var proxyFile = Source.fromFile("proxy.lst").getLines.toList
        var proxy:Proxy = null
        if(proxyList.nonEmpty) {
            // if we have proxies, use one when requesting the list of proxies.
            // better safe than sorry :^)
            proxy = proxyList(rand.nextInt(proxyList.length))
            req.setProxyServer(new ProxyServer(proxy.ip, proxy.port))
        }
        
        println("Finding more proxies...")
        var newProxyList: Array[Proxy] = Array[Proxy]()
        for (rawProxy <- proxyFile) {
                       
            if (rawProxy.indexOf(":") != -1 && rawProxy.indexOf("*") == -1) {
                val proxyArr = rawProxy.split(":")
                val proxy = Proxy(proxyArr(0), proxyArr(1).toInt)
                if(!proxyList.contains(proxy)) newProxyList = newProxyList :+ proxy
            }
        }                

        testProxies(newProxyList)
    }

    def testProxies(newProxyList: Array[Proxy]) = {
        println("Testing new proxies.")

        for(proxy <- newProxyList) {
            //val req: Req = createRequestObject.setProxyServer(new ProxyServer(proxy.ip, proxy.port))
            
            val req: Req = createRequestObject
          
            val responseFuture = http(req OK as.String)
            
            responseFuture.onSuccess{
                case response =>
                    // check if we got a proxy filter screen
                    if(response.toLowerCase.indexOf("path of exile shops indexer") != -1) {
                        proxyList = proxyList :+ proxy
                        println("Adding proxy", proxy.ip, proxy.port)
                    }
            }
            responseFuture.onFailure {
                case e =>
                    println("Couldn't fetch the proxy list site.",e)
            }
        }
    }

    def createRequestObject: Req = {
        Http.configure(_ setFollowRedirects true)
        val fields = Map("buyout" -> "x")
        val params = List("league" -> "Talisman", "online" -> "x", "capquality" -> "x") ++ fields
        val req = url("http://poe.trade/search").POST << params
        req
    }

    def schedule = {
        //ActorSystem("proxyfinder").scheduler.schedule(0 seconds, 10 seconds)({
            if(proxyList.length < minimumProxies) findMoreProxies()
        //})
    }

    def main(args: Array[String]) {
        schedule
    }
}
